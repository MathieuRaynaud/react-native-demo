import { StyleSheet } from 'react-native';

const primaryColor = "#2196f3"
const primaryLightColor = "#6ec6ff"
const primaryDarkColor = "#0069c0"
const secondaryColor = "#76ff03"
const secondaryLightColor = "#b0ff57"
const secondaryDarkColor = "#32cb00"
const primaryTextColor = "#fafafa"
const secondaryTextColor = "#000000"

module.exports = StyleSheet.create({
    headerStyle:{
        backgroundColor: primaryColor,
        height: 80
    },
    headerTitleStyle:{
        color: primaryTextColor,
        fontSize: 30,
    },
    buttonStyle:{
        marginTop: 20,
        marginRight: 40,
        marginBottom: 20,
        marginLeft: 40
    },
    mainContainerStyle:{
        flex: 1,
        margin: 20
    }
})