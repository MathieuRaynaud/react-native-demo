import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { Icon } from 'react-native-elements';
import Home from './Home';
import Map from './Map';

import { useIsFocused } from '@react-navigation/native';

import mainStyle from '../styles/mainStyle';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const BottomTabNavigator = () => {

  const HomeStack = ({navigation}) => {
    return(
      <Stack.Navigator>
        <Stack.Screen
        name="Home"
        component={Home}
        options={() => ({
          title: 'Accueil',  // Title to appear in status bar
          headerStyle: mainStyle.headerStyle,
          headerTitleStyle: mainStyle.headerTitleStyle
        })}
        />
      </Stack.Navigator>
    )
  }

  const MapStack = ({navigation}) => {
    return(
      <Stack.Navigator>
        <Stack.Screen
        name="Map"
        component={Map}
        options={() => ({
          title: 'Carte',  // Title to appear in status bar
          headerStyle: mainStyle.headerStyle,
          headerTitleStyle: mainStyle.headerTitleStyle
        })}
        />
      </Stack.Navigator>
    )
  }

  return (
    <Tab.Navigator
    initialRouteName="HomeScreen"
    tabBarOptions={{
          activeTintColor: '#0069c0'
    }}
    >
        <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        options={({navigation}) => ({
            tabBarLabel: 'Accueil',
            tabBarIcon: () => (
            <Icon
                onPress={() => {
                    navigation.navigate('HomeStack');
                }}
                type='material-community' name="home" color={useIsFocused() ? '#0069c0' : '#32cb00'} />
            )
        })}
        />
        <Tab.Screen
        name="MapStack"
        component={MapStack}
        options={({navigation}) => ({
            tabBarLabel: 'Carte',
            tabBarIcon: () => (
            <Icon
                onPress={() => {
                    navigation.navigate('MapStack');
                }}
                type='material-community' name="map" color={useIsFocused() ? '#0069c0' : '#32cb00'} />
            )
        })}
        />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;