import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import mainStyle from '../styles/mainStyle';

const Map = () => {

  return (
        <View style={mainStyle.mainContainerStyle}>
          <Text>Map page</Text>
        </View>
  );
}

export default Map