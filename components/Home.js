import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import mainStyle from '../styles/mainStyle';

const Home = ({navigation}) => {

  return (
    <View style={mainStyle.mainContainerStyle}>
      <Text>Accueil</Text>
    </View>
  );
}

export default Home